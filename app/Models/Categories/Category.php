<?php

namespace App\Models\Categories;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function apiUrl()
    {
        return  url('api/v1/categories/'.$this->id);
    }
}
