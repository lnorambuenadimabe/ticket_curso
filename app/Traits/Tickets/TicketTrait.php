<?php
namespace App\Traits\Tickets;

use App\Mail\TicketStore;
use App\Notifications\TicketStoreNotification;
use Carbon\Carbon;

trait TicketTrait
{
    public function time()
    {
        return Carbon::now()->diffInDays($this->created_at);
    }

    public function apiUrl()
    {
        return url('api/v1/tickets/'.$this->id);
    }


    public function addFile()
    {
        if (request()->hasFile('file')) {
            $path = request()->file->storeAs('',request()->file->getClientOriginalName(),'public');

            $this->files()->create(['name' => request()->file->getClientOriginalName(), 'path' => $path]);
        }
        return $this;
    }

    public function sendMail()
    {
        \Mail::to(request()->user())->send(new TicketStore());
    }

    public function sendNotification()
    {
        auth('api')->user()->notify(new TicketStoreNotification($this));
    }
}