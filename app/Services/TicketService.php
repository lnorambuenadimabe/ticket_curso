<?php
namespace App\Services;
use App\Http\Resources\Tickets\TicketResource;
use App\Models\Tickets\Ticket;

class TicketService
{

    public function store($request)
    {

        $ticket = Ticket::create($request->only('title', 'details'));

        $ticket->addFile()->sendNotification();



        return new TicketResource($ticket);
    }

}