<?php

namespace App\Http\Controllers\Tickets;

use App\Http\Requests\Ticket\CommentStoreRequest;
use App\Http\Resources\Tickets\CommentResource;
use App\Models\Tickets\Comment;
use App\Models\Tickets\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketCommentController extends Controller
{
    public function store(CommentStoreRequest $request, Ticket $ticket)
    {
        $comment = $ticket->comments()->create(['comment' => $request->comment]);
        return response()->json(['data' => $comment], 201);
    }
    public function index(Ticket $ticket)
    {
        $comments = $ticket->latestById();
        return CommentResource::collection($comments);
    }
    public function destroy(Ticket $ticket, Comment $comment)
    {
        $this->authorize('delete',$comment);

         if($ticket->comments()->where('id',$comment->id)->first() == null)
         {
             return response()->json(['error'=>'comentario no pertenece al ticket de la petición'], 400);
         }
        $comment->delete();
        return response()->json([], 204);
    }
    public function update(Ticket $ticket,Comment $comment)
    {
        $comment->update(request()->only('comment'));
        return response()->json(['data'=>$comment],200);
    }
}
