<?php

namespace App\Http\Controllers\Tickets;

use App\Http\Requests\Ticket\TicketStoreRequest;
use App\Http\Requests\Ticket\UpdateTicketRequest;
use App\Http\Resources\Tickets\TicketResource;
use App\Models\Tickets\Ticket;
use Facades\App\Services\TicketService as TicketFacades;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{
    /*public $ticketService;

     public function __construct(TicketService $ticketService)
     {
         $this->ticketService = $ticketService;
     }*/

    public function index()
    {

        $tickets = Ticket::with('author')->title()->paginateIf();


        return TicketResource::collection($tickets);
    }

    public function show(Ticket $ticket)
    {
        $ticket->load('author');
        return new TicketResource($ticket);
    }

    public function store(TicketStoreRequest $request)
    {
        // return $this->>ticketService->store($request);  // inyectando el servicio
        return TicketFacades::store($request); // utilizando una fachada en tiempo real
    }

    public function update(UpdateTicketRequest $request, Ticket $ticket)
    {
        $ticket->update($request->only('title', 'details'));
        return new TicketResource($ticket);

    }

    public function destroy(Ticket $ticket)
    {
        $ticket->delete();

        return response()->json([], 204);
    }
}
