<?php

namespace App\Http\Controllers\Categories;

use App\Models\Categories\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function store(Request $request)
    {
        $category = Category::create($request->only('name'));
        return response()->json(['data'=>$category],201);
    }
}
