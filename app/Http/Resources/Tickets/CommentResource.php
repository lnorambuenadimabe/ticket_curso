<?php

namespace App\Http\Resources\Tickets;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\Resource;

class CommentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'comment' => $this->comment,
            'ticket_id' => $this->ticket_id,
            'author' => new UserResource($this->whenLoaded('author')),
        ];
    }
}
