<?php

namespace App\Http\Resources\Tickets;

use App\Http\Resources\UserResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class TicketResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'details' => $this->details,
            'created_at' => Carbon::parse($this->created_at)->format('d/m/Y H:i'),
            'time' => $this->time(),
            'url' => $this->apiUrl(),
            'author' => new UserResource($this->whenLoaded('author')),
        ];
    }
}
