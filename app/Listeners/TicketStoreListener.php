<?php

namespace App\Listeners;

use App\Services\TicketService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketStoreListener
{
    public $ticketService;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->ticketService->store($event->data);
    }
}
