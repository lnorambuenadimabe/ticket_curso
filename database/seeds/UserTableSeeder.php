<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::create([
           'name' => 'admin',
           'email' => 'prueba@test.com',
           'password' => bcrypt(1234),
           'api_token' => str_random(100)
        ]);

        factory(\App\Models\Tickets\Ticket::class,50)->create();
    }
}
