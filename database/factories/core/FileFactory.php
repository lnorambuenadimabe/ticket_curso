<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Files\File::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'path' => $faker->url
    ];
});
