<?php

use Faker\Generator as Faker;
use App\Models\Tickets\Ticket;
use App\Models\User;

$factory->define(Ticket::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->word,
        'details' => $faker->paragraph,
        'author_id' => function () {
            return factory(User::class)->create()->id;
        }
    ];
});
