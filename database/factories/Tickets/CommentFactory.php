<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Tickets\Comment::class, function (Faker $faker) {
    return [
        'author_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
        'ticket_id' => function() {
            return factory(\App\Models\Tickets\Ticket::class)->create()->id;
        },
        'comment' => $faker->paragraph
    ];
});
