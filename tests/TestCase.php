<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;
use Mockery\Generator\StringManipulation\Pass\Pass;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    function signIn($data = [])
    {
        $user = factory(User::class)->create($data);
        Passport::actingAs($user);

        return $user;
    }
}
