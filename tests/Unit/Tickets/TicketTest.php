<?php

namespace Tests\Unit\Tickets;

use App\Models\Files\File;
use App\Models\Tickets\Comment;
use App\Models\Tickets\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TicketTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->signIn();
    }

    function test_time_from_ticket()
    {
        $ticket = factory(Ticket::class)->create([
            'created_at' => Carbon::now()->subDays(5)
        ]);

        $this->assertTrue($ticket->time() == 5);

    }

    function test_url_from_ticket()
    {
        $ticket = factory(Ticket::class)->create();

        $this->assertTrue(url('api/v1/tickets/'.$ticket->id) == $ticket->apiUrl());
    }

    function test_a_ticket_has_files()
    {
        $ticket = factory(Ticket::class)->create();
        $files = factory(File::class,2)->create([
            'fileable_id' => $ticket->id,
            'fileable_type' => Ticket::class
        ]);

        $this->assertInstanceOf(Collection::class, $ticket->files);
        $this->assertInstanceOf(File::class, $ticket->files()->first());
    }

    function test_ticket_has_author()
    {
        $user = $this->signIn();

        $ticket = factory(Ticket::class)->create([
            'author_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $ticket->author);
        $this->assertTrue($ticket->author->id == $user->id);
    }

    function test_ticket_has_comments()
    {
        $ticket = factory(Ticket::class)->create();
        factory(Comment::class, 3)->create([
            'ticket_id' => $ticket->id
        ]);

        $this->assertInstanceOf(Collection::class, $ticket->comments);
        $this->assertInstanceOf(Comment::class, $ticket->comments()->first());
    }
}
