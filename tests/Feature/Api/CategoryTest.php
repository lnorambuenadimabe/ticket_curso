<?php

namespace Tests\Feature\Api;

use App\Models\Categories\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_store_category()
    {
        $this->signIn();
        $parameters = [
            'name' => factory(Category::class)->make()->name
        ];
        $this->post('api/v1/categories',$parameters)
         //  ->assertStatus(201)
            ->assertJson([
                'data'=>[
                    'name' => $parameters['name']
                ]
            ]);
    }
}
