<?php

namespace Tests\Feature\Api;

use App\Mail\TicketStore;
use App\Models\Tickets\Ticket;
use App\Models\User;
use App\Notifications\TicketStoreNotification;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TicketsTest extends TestCase
{
    use RefreshDatabase;

    function test_lists_tickets()
    {
        //when
        $this->signIn();
        $ticket = factory(Ticket::class)->create();

        //then

        $this->get('api/v1/tickets')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    0 => [
                        'title' => $ticket->title,
                        'author' => [
                            'id' => $ticket->author->id
                        ]
                    ]
                ]
            ]);
    }

    function test_paginate_tickets()
    {
        $this->signIn(); // se crea un usuario de prueba
        factory(Ticket::class, 15)->create();

        $parameters = ['page'=>1];

        $this->call('GET', 'api/v1/tickets',$parameters)
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'total'=> 15
                ]
            ]);


    }

    function test_paginate_tickets_by_page()
    {
        $this->signIn(); // se crea un usuario de prueba
        factory(Ticket::class,15)->create();
        $ticket = factory(Ticket::class)->create();

        $parameters = ['page'=>2];

        $this->call('GET', 'api/v1/tickets',$parameters)
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'total'=> 16
                ],
                'data' => [
                    0 =>[
                        'id' => $ticket->id
                    ]

                ]
            ]);


    }

    function test_show_a_ticket()
    {
        $this->signIn(); // se crea un usuario de prueba
        $ticket = factory(Ticket::class)->create();

        $this->get($ticket->apiUrl())
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                        'title' => $ticket->title
                ]
            ]);

    }

    function test_search_ticket_by_title()
    {
        $this->signIn();
        $ticket = factory(Ticket::class)->create();

        $parameter = ['title' => $ticket->title, 'page' => 1];

        $this->call('GET', 'api/v1/tickets', $parameter)
                ->assertStatus(200)
                ->assertJson([
                    'meta' => [
                        'total' => 1
                    ],
                    'data' => [
                        0 => [
                            'title' => $ticket->title
                        ]

                    ]
                ]);
    }

    function test_validate_a_ticket_at_store()
    {
        $this->signIn(); // se crea un usuario de prueba

        $this->postJson('api/v1/tickets')
            ->assertStatus(422) // si no se agrega el postJson devolvería un 322
            ->assertJson([
                'errors' => [
                    'title' => ['The title field is required.'],
                    'details' => ['The details field is required.']
                ]
            ]);
    }

    function test_validate_a_title_be_unique()
    {
        $this->signIn();
        $ticket = factory(Ticket::class)->create(['title' => 'titulo']);

        $this->postJson('api/v1/tickets', $ticket->toArray())
            ->assertJson([
                'errors' => [
                    'title' => ['The title has already been taken.']
                ]
            ]);


    }

    function test_store_a_ticket()
    {
        //when

        Storage::fake();

        Notification::fake();
        //Mail::fake();

        $user =  $this->signIn(); // se crea un usuario de prueba


        $parameters = [ // se crean los parametros para la creación de un ticket
            'title' => 'prueba',
            'details' => 'prueba2',
            'file' => UploadedFile::fake()->create('test.pdf')
        ];

        //then

        $this->post('api/v1/tickets', $parameters)
                //->assertStatus(201)
                ->assertJson([
                    'data' => [
                        'title' => $parameters['title']
                    ]
                ]);

        // comprobar si el archivo fue subido
        Storage::disk('public')->assertExists('test.pdf');

        $this->assertDatabaseHas('files', [
            'name' => 'test.pdf'
        ]);

        // se comprueba si el registro fue guardado en la base de datos
        $this->assertDatabaseHas('tickets', array_except($parameters, ['file']));

        Notification::assertSentTo([$user], TicketStoreNotification::class);
    }

    function test_validate_a_ticket_at_update()
    {
        $this->signIn(); // se crea un usuario de prueba
        $ticket = factory(Ticket::class)->create();

        $this->putJson($ticket->apiUrl(), [])
            ->assertStatus(422) // si no se agrega el postJson devolvería un 322
            ->assertJson([
                'errors' => [
                    'title' => ['The title field is required.'],
                    'details' => ['The details field is required.']
                ]
            ]);
    }

    function test_update_a_ticket()
    {
        $this->signIn();

        $parameters = [ // se crean los parametros para la creación de un ticket
            'title' => 'titulo actualizado',
            'details' => 'detalle actualizado'
        ];

        $ticket = factory(Ticket::class)->create();

        //then

        $this->put($ticket->apiUrl(), $parameters)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => $parameters['title'],
                    'id' => $ticket->id
                ]
            ]);
        // se comprueba si el registro fue guardado en la base de datos
        $this->assertDatabaseHas('tickets', $parameters);


    }

    function test_destroy_a_ticket()
    {
        $this->signIn();

        $ticket = factory(Ticket::class)->create();

        $this->delete($ticket->apiUrl())
            ->assertStatus(204);
        $this->assertSoftDeleted('tickets', [
            'id' => $ticket->id
        ]);
    }
}
