<?php

namespace Tests\Feature\Api;

use App\Models\Tickets\Comment;
use App\Models\Tickets\Ticket;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    function test_create_a_comment()
    {
        // when
        $user = $this->signIn();

        $ticket = factory(Ticket::class)->create();

        $comment = factory(Comment::class)->make(['ticket_id' => $ticket->id, 'author_id' => $user->id]);

        // then
        $this->post("api/v1/tickets/{$ticket->id}/comments", $comment->toArray())
                ->assertStatus(201)
                ->assertJson([
                    'data' => [
                        'comment' => $comment->comment
                    ]
                ]);
        $this->assertDatabaseHas('comments', $comment->toArray());
    }
    function test_list_comments_from_ticket()
    {
        $user = $this->signIn();
        $ticket = factory(Ticket::class)->create();
        factory(Comment::class,3)->create(['ticket_id'=>$ticket->id,'author_id'=>$user->id]);
        $comment = factory(Comment::class)->create(['ticket_id'=>$ticket->id,'author_id'=>$user->id]);
        $this->get($ticket->apiUrl() . '/comments')
            ->assertJson([
                'meta' =>[
                    'total' => 4
                ],
                'data'=>[
                    0=> [
                        'comment'=> $comment->comment
                    ]
                ]
            ]);
    }
    function test_destroy_a_comment_from_ticket()
    {
        $user = $this->signIn();
        $ticket = factory(Ticket::class)->create(['author_id'=>$user->id]);
        $comment = factory(Comment::class)->create(['ticket_id' => $ticket->id,'author_id'=> $user->id]);

        $this->delete($ticket->apiUrl() . '/comments/'. $comment->id)
            ->assertStatus(204);
        $this->assertDatabaseMissing('comments', [
                'id' => $comment->id
            ]);
    }
    function test_only_owner_can_destroy_comment()
    {
        $owner = $this->signIn();
        $ticket = factory(Ticket::class)->create();
        $comment = factory(Comment::class)->create(['ticket_id' => $ticket->id]);
        $user = $this->signIn();
        $this->delete($ticket->apiUrl() . '/comments/'. $comment->id)
            ->assertStatus(403);
        $this->assertDatabaseHas('comments', [
            'id' => $comment->id
        ]);
    }
    function test_comment_does_not_belong_to_a_ticket()
    {
        $user = $this->signIn();
        $ticket = factory(Ticket::class)->create(['author_id'=>$user->id]);
        $comment = factory(Comment::class)->create(['author_id'=>$user->id]);

        $this->delete($ticket->apiUrl() . '/comments/'. $comment->id)
            ->assertStatus(400);
        $this->assertDatabaseHas('comments', [
            'id' => $comment->id
        ]);
    }

    function test_update_a_comment_from_ticket()
    {
        $user = $this->signIn();
        $ticket = factory(Ticket::class)->create(['author_id'=>$user->id]);
        $comment = factory(Comment::class)->create(['ticket_id' => $ticket->id,'author_id'=> $user->id]);
        $parameters = [
            'comment' => 'nuevo valor'
        ];
        $this->put($ticket->apiUrl() . '/comments/'. $comment->id,$parameters)
            ->assertStatus(200)
            ->assertJson([
            'data'=>[
                'comment' => 'nuevo valor'
            ]
        ]);
    }
}
