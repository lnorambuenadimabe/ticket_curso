class Ticket {
    static fetch(params, then) {
        axios.get('/api/v1/tickets', {
            params: params
        }).then(({data}) => then(data));
    }
}

export default Ticket;
