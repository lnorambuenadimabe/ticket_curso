<?php

use Illuminate\Http\Request;

Route::namespace('Tickets')
    ->middleware('auth:api')
    ->group(base_path('routes/Api/tickets.php'));

Route::namespace('Categories')->middleware('auth:api')->group(base_path('routes/Api/categories.php'));

Route::post('broadcasting/auth', function(Request $request) {
    echo \Pusher\Laravel\Facades\Pusher::socket_auth($request->channel_name, $request->socket_id);
});

Route::get('auth', function () {
    return auth('api')->user();
})->middleware('auth:api');