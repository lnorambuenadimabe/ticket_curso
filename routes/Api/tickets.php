<?php

Route::apiResource('tickets', 'TicketController');

Route::apiResource('tickets.comments', 'TicketCommentController');